package com.jeesuite.monitor.common.asm;

public interface MethodInjectHandlerFactory {
	
	public MethodInjectHandler getMethodHandler(String className, String methodName, String signature);
   
}
