package com.jeesuite.monitor.model.metaobject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 *轨迹基类
 * <p>
 * 轨迹用于封装对性能分析有用的信息（比如上下文的信息，线程堆栈的信息，执 行时间的信息等等）。
 * </p>
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2014年6月14日
 */
public class Trace implements Serializable, Comparable<Trace>,Cloneable{
	
	private static final long serialVersionUID = 1L;
	
	public static enum TraceType {
		METHOD,JDBC,HTTP;
	}

	//trace 标识
	protected String traceId;
	//同一个线程该值相同）
	protected String threadId;//线程ID
	
	protected String traceType;

	/**
	 * 创建时间
	 */
	protected long beginTime;

	/**
	 * 结束时间
	 */
	protected long endTime;

	/**
	 * 轨迹内容
	 */
	protected String content;

	/**
	 * 父轨迹
	 */
	protected Trace parent;
	
	protected boolean timeout;//标记为超时
	
	protected boolean destroy;//标记为已销毁

	/**
	 * 子轨迹
	 */
	protected List<Trace> traces;

	public Trace() {
		init(parent);
	}

	public Trace(Trace parent) {
		init(parent);
	}

	private void init(Trace parent) {
		if (parent != null)
			parent.addChild(this);
		this.parent = parent;
		beginTime = System.currentTimeMillis();
		traceId = UUID.randomUUID().toString().replaceAll("\\-", "");
	}
	

	/**
	 * 增加子轨迹
	 * 
	 * @param trace
	 */
	public void addChild(Trace trace) {
		if(this.traces == null)this.traces = new ArrayList<Trace>();
		synchronized (traces) {
			this.traces.add(trace);
		}
	}


	/**
	 * 得到子轨迹
	 * 
	 * 返回定义为数组有2个目的:
	 * 
	 * 1)返回复本，保证外在的操作不会侵入轨迹的内部运行
	 * 
	 * 2)返回类型为数组，更不易产生歧义，误以为得到的是轨迹内部的集合结构
	 * 
	 * @return
	 */
	public Trace[] getChildTraces() {
		Trace[] ts = null;
		if(traces != null){
			ts = new Trace[traces.size()];
			traces.toArray(ts);
		}else{
			ts = new Trace[0];
		}
		return ts;
	}

	/**
	 * 轨迹存活时间
	 * 
	 * @return
	 */
	public long getActiveTime() {
		return getEndTime() <= 0L ? System.currentTimeMillis()
				- getBeginTime() : getEndTime() - getBeginTime();
	}

	/**
	 * 清空子轨迹
	 */
	public synchronized void clearChildTrace() {
		if (traces != null)
			traces.clear();
	}

	/**
	 * 删除子轨迹
	 * 
	 * @param trace
	 */
	protected synchronized void removeChildTrace(Trace trace) {
		if (this.traces != null) {
			this.traces.remove(trace);
			trace.parent = null;
		}
	}

	/**
	 * 销毁轨迹
	 */
	public void destroy() {
		destroy = true;
		if (parent != null) {
			parent.removeChildTrace(this);
			parent = null;
		}
		Trace[] children = getChildTraces();
		clearChildTrace();
		for (int i = 0; i < children.length; i++) {
			Trace trace = children[i];
			trace.destroy();
		}
	}

	/**
	 * 结束
	 */
	public void inActive() {
		if (!(endTime > 0L))
			endTime = System.currentTimeMillis();
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getTraceId() {
		return traceId;
	}

	public void setTraceId(String traceId) {
		this.traceId = traceId;
	}

	public String getThreadId() {
		return threadId;
	}

	public void setThreadId(String threadId) {
		this.threadId = threadId;
	}

	public String getTraceType() {
		return traceType;
	}

	public void setTraceType(String traceType) {
		this.traceType = traceType;
	}

	public long getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(long beginTime) {
		this.beginTime = beginTime;
	}

	public long getEndTime() {
		return endTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	public boolean isTimeout() {
		return timeout;
	}

	public void setTimeout(boolean timeout) {
		this.timeout = timeout;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((traceId == null) ? 0 : traceId.hashCode());
		return result;
	}
	
	public boolean destroyed() {
		return destroy;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Trace other = (Trace) obj;
		if (traceId == null) {
			if (other.traceId != null)
				return false;
		} else if (!traceId.equals(other.traceId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

	@Override
	public int compareTo(Trace o) {
		return (int)(this.beginTime - o.getBeginTime());
	}
	
	public Trace clone(){
		try {
			return (Trace) super.clone();
		} catch (CloneNotSupportedException e) {
			return null;
		}
	}
	
}
