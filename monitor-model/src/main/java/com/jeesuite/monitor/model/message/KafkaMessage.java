package com.jeesuite.monitor.model.message;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 *  默认消息实体
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2016年4月19日
 */
public class KafkaMessage implements Serializable {

	private static final long serialVersionUID = 1L;
	private String msgId = UUID.randomUUID().toString();
	private Serializable partitionFactor;//分区因子
	private Map<String, Object> headers;
	
	private Serializable body;

	public KafkaMessage(Serializable body) {
		super();
		this.body = body;
	}
	
	public String getMsgId() {
		return msgId;
	}

	public Serializable getBody() {
		return body;
	}

	public KafkaMessage body(Serializable body) {
		this.body = body;
		return this;
	}

	public Serializable getPartitionFactor() {
		return partitionFactor;
	}

	public KafkaMessage partitionFactor(Serializable partitionFactor) {
		this.partitionFactor = partitionFactor;
		return this;
	}


	public Map<String, Object> getHeaders() {
		return headers;
	}

	public KafkaMessage header(String key,Object value) {
		if(this.headers == null)this.headers = new HashMap<>();
		this.headers.put(key, value);
		return this;
	}
	
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}


}
