package com.jeesuite.monitor.model.config;

import com.jeesuite.monitor.model.custom.PropertyStorage;

public class ChannelDef  extends PropertyStorage{

	private static final long serialVersionUID = 1L;
	
	private String type;


	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	

}
