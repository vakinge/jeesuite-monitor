package com.jeesuite.monitor.model.config;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class NodeDef implements Serializable, Cloneable{

	private static final long serialVersionUID = -3451267566331657691L;
	/**
	 * plugin定义引用
	 * 
	 * @see PluginDefRef
	 */
    private String id;
	private String uri;
	private String name; 
	
	private List<ComponentDef> components = null;
	
	private List<TaskDef> tasks = null;
	
	private ChannelDef channel;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public NodeDef() {
		components = new ArrayList<ComponentDef>();
	}

	public List<ComponentDef> getComponents() {
		return components;
	}

	public void setComponents(List<ComponentDef> components) {
		this.components = components;
	}

	/**
	 * 增加Component定义
	 * @param name
	 * @param component
	 */
	public void addComponentDef(ComponentDef component) {
		components.add(component);
	}

	/**
	 * 获得Component定义
	 * @param name
	 * @return
	 */
	public ComponentDef getComponentDef(String type) {
		for (ComponentDef c : components) {
			if(c.getType().equals(type))return c;
		}
		return null;
	}


	public List<TaskDef> getTasks() {
		return tasks;
	}

	public void setTasks(List<TaskDef> tasks) {
		this.tasks = tasks;
	}

	public ChannelDef getChannel() {
		return channel;
	}

	public void setChannel(ChannelDef channel) {
		this.channel = channel;
	}

	public Object clone(){
		NodeDef defCopy=new NodeDef();
		
		return defCopy;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("[");
		sb.append("\n\tid=" + id );
		sb.append("\n\tComponent{");
		for (ComponentDef c : components) {
			sb.append("\n\t\t"+c);
		}
		sb.append("\n\t}");
		sb.append("\nchannel=" + channel.getType() );
		sb.append("\n]");
		return sb.toString();
	}
	
	
}
