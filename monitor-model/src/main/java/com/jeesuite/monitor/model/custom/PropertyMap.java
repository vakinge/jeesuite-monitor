package com.jeesuite.monitor.model.custom;

import java.util.Map;

/**
 * 参数集合对象，通常由字符串组成的name和value键值对组成
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2016年6月14日
 */
public interface PropertyMap{

	public String getProperty(String key);

	public String[] propNames();
	
	public void putProperty(String key, String value);
	
	public void putProperties(Map<String, String> props);

}
