package com.jeesuite.monitor.core;

import com.jeesuite.monitor.model.metaobject.Trace;


/**
 * 轨迹分析器
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2016年6月15日
 */
public interface Analyser {
	
	public void activeProcess(Trace trace);

	public void inactiveProcess(Trace trace);

	public void destoryProcess(Trace trace);

	public void clear();
}
