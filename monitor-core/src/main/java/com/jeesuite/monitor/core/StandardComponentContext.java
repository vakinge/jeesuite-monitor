package com.jeesuite.monitor.core;

import com.jeesuite.monitor.model.config.ComponentDef;

/**
 * ComponentContext标准实现
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2016年6月15日
 */
public class StandardComponentContext extends AbstractContext implements ComponentContext{

	/**
	 * 轨迹容器
	 */
	private TraceManager _container=null;
	
	private Component _component=null;
	
	public Component getComponent() {
		return _component;
	}

	public void setComponent(Component component) {
		this._component = component;
	}

	public StandardComponentContext(TraceManager container,Context parent,ComponentDef def){
		super(parent,def);
		_container=container;
	}
	
	
	
	public TraceManager getContainer() {
		return _container;
	}
}
