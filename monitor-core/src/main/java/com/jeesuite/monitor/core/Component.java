package com.jeesuite.monitor.core;


/**
 * 监控组件接口
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2016年6月15日
 */
public interface Component {
	
	/**
	 * plug-in启动方法，实现者可以在该方法中初始组件需要的资源
	 * 该方法由Jwebap启动时调用
	 * @param context
	 */
	public void startup(ComponentContext context);
	
	/**
	 * 该方法在Jwebap移除组件时调用，实现者可以在这里进行资源的回收
	 *
	 */
	public void destory();
	
	/**
	 * 该方法通常由用户在界面触发调用，用以清理一些组件内部使用的临时数据，以及统计数据，
	 * 清理完成，组件的状态应该归为刚刚启动时的状态
	 *
	 */
	public void clear();
	
	/**
	 * 获得组件上下文
	 *
	 */
	public ComponentContext getComponentContext();
}
