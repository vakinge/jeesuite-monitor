package com.jeesuite.monitor.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.jeesuite.monitor.model.metaobject.CombineMethodTrace;
import com.jeesuite.monitor.model.metaobject.MethodTrace;
import com.jeesuite.monitor.model.metaobject.Trace;
import com.jeesuite.monitor.model.metaobject.Trace.TraceType;

/**
 * 
 * 功能描述：轨迹数据生命周期管理<br />
 *  
 * 创建日期：2013-6-25 上午11:29:51  <br />   
 * 
 * 版权信息：Copyright (c) 2013 Koala All Rights Reserved<br />
 * 
 * 作    者：<a href="mailto:vakinge@gmail.com">vakin jiang</a><br />
 * 
 * 修改记录： <br />
 * 修 改 者    修改日期     文件版本   修改说明
 */
public class TraceContainer implements TraceManager{
	
	private static ThreadLocal<String> threads = new ThreadLocal<String>();
	
	protected Map<String ,List<Analyser>> analysers=null;
	
	private abstract class DoProcess{
		void doProcess(String traceType, Trace trace){
			Collection<Analyser> as= analysers.get(traceType);
			if(as==null){
				return;
			}
			Iterator<Analyser> it=as.iterator();
			while(it.hasNext()){
				Analyser analyser=it.next();
				if(trace == null)continue;
				doEvent(analyser,trace);
			}
		}
		abstract void doEvent(Analyser analyser,Trace trace);
	};
	
	public TraceContainer(){
		analysers=new HashMap<String ,List<Analyser>>();
	}
	
	public void activateTrace(String traceType, Trace trace) {
		
		if(!MonitorContext.componentIsActive(traceType))return;
		//设置线程标识
		if(trace.getThreadId() == null){
			trace.setThreadId(getCurrentThreadKey());
		}
		trace.setTraceType(traceType);
		
		DoProcess process=new DoProcess(){
			void doEvent(Analyser analyser,Trace trace){
				analyser.activeProcess(trace);
			}		
		};
		process.doProcess(traceType,trace);	
	}

	public void inactivateTrace(String traceType,Trace trace) {
		
		if(trace == null)return;
		if(!MonitorContext.componentIsActive(traceType))return;
		
		DoProcess process=new DoProcess(){
			void doEvent(Analyser analyser,Trace trace){
				analyser.inactiveProcess(trace);
			}		
		};
		process.doProcess(traceType,trace);	
		
		if(trace.destroyed())return;//已经销毁轨迹不处理

		//放入交换数据缓存区
		if(TraceType.METHOD.name().equals(trace.getTraceType())){
			CombineMethodTrace combineMethodTrace = new CombineMethodTrace();
			combineMethodTrace.combineTraceInfo((MethodTrace)trace);
			MonitorContext.getContext().getChannel().process(combineMethodTrace);
		}else{
			//放入clone对象
			MonitorContext.getContext().getChannel().process(trace.clone());
		}
		
		//销毁
		trace.destroy();
		destoryTrace(traceType, trace);
		trace = null;
	}


	public void destoryTrace(String traceType,Trace trace) {
		DoProcess process=new DoProcess(){
			void doEvent(Analyser analyser,Trace trace){
				analyser.destoryProcess(trace);
			}		
		};
		process.doProcess(traceType,trace);	
	}

	public void registerAnalyser(String traceType,Analyser analyser) {
		if(analysers.get(traceType)==null){
			analysers.put(traceType,new ArrayList<Analyser>());
		}
		Collection<Analyser> as= analysers.get(traceType);
		as.add(analyser);
	}

	public void unregisterAnalyser(String traceType,Analyser analyser) {
		Collection<Analyser> as= analysers.get(traceType);
		if(as!=null){
			as.remove(analyser);
		}
	}
	

	/**
	 * 获取当前线程标识
	 * @return
	 */
	public static String getCurrentThreadKey(){
		String key = threads.get();
		if(key == null){
			key = UUID.randomUUID().toString().replaceAll("\\-", "");
			threads.set(key);
		}
		return key;
	}
	
	public static void clearThreadKey(){
		threads.set(null);
	}
	
}
