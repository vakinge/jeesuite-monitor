package com.jeesuite.monitor.core;

import com.jeesuite.monitor.model.metaobject.Trace;


/**
 * 轨迹收集器
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2016年6月15日
 */
public interface TraceManager {
	public void activateTrace(String traceType,Trace trace);
	public void inactivateTrace(String traceType,Trace trace);
	public void destoryTrace(String traceType,Trace trace);
	public void registerAnalyser(String traceType,Analyser analyser);
	public void unregisterAnalyser(String traceType,Analyser analyser);

}
