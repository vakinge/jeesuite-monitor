package com.jeesuite.monitor.core;

import java.util.Map;

/**
 * 上下文接口
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2016年6月15日
 */
public interface Context {

	public Context getParent();
	
	public void initProperties(Map<String, String> properties);
	
	public void addProperty(String propName,String propValue);
	
	public String getProperty(String propName);
}
