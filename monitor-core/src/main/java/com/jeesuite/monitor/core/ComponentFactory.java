package com.jeesuite.monitor.core;

import com.jeesuite.monitor.component.http.HttpComponent;
import com.jeesuite.monitor.component.jdbc.JdbcComponent;
import com.jeesuite.monitor.component.method.MethodComponent;
import com.jeesuite.monitor.model.metaobject.Trace.TraceType;


/**
 * 监控组件工厂
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2016年6月15日
 */
public class ComponentFactory {
	
	public static Component getInstance(String type){
		Component component = null;
		if(TraceType.HTTP.name().equals(type)){
			component = new HttpComponent();
		}else if(TraceType.METHOD.name().equals(type)){
			component = new MethodComponent();
		}else if(TraceType.JDBC.name().equals(type)){
			component = new JdbcComponent();
		}
		return component;
	}
}
