package com.jeesuite.monitor.core;


/**
 * Component组件注册异常
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2016年6月15日
 */
public class RegisterException extends Exception{
	
	public RegisterException(String msg){
		super(msg);
	}
	
	public RegisterException(String msg,Throwable cause){
		super(msg,cause);
	}
}
