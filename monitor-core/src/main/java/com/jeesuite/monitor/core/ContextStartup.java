package com.jeesuite.monitor.core;

import java.util.Map;

import com.jeesuite.monitor.exception.ContextInitialException;
import com.jeesuite.monitor.model.config.NodeDef;

/**
 * 监控Context启动器
 * @author Administrator
 *
 */
public class ContextStartup {

	/**
	 * 启动监控contentext
	 * @param configPath
	 * @param serverInfos
	 */
	public static void startup(String configPath,Map<String, String> serverInfos)  {
		NodeDef config = null;
		ConfigureManager defManager=new ConfigureManager(configPath);
		
		try {
			config = defManager.get();
		} catch (Exception e) {
			throw new ContextInitialException("",e);
		}
		

		MonitorContext context = MonitorContext.registerContext(config,defManager);
		context.registerServerInfos(serverInfos);
		context.startup();

	}
}
