package com.jeesuite.monitor.core;

import java.util.HashMap;
import java.util.Map;

import com.jeesuite.monitor.model.custom.PropertyMap;

/**
 * 抽象上下文
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2016年6月15日
 */
public abstract class AbstractContext implements Context{
	
	private Context _parent=null;
	
	protected Map<String, String> properties=new HashMap<String, String>();
	
	public AbstractContext(){
	}
	
	public AbstractContext(Context parent){
		_parent=parent;
	}
	
	public AbstractContext(Context parent,PropertyMap map){	
		_parent=parent;

		String[] names=map.propNames();
		for(int i=0;i<names.length;i++){
			properties.put(names[i],map.getProperty(names[i]));
		}
	
	}
	
	public Map<String, String> getProperties() {
		return properties;
	}

	public void initProperties(Map<String, String> properties) {
		if(properties != null){
			this.properties.putAll(properties);
		}
	}
	
	public void addProperty(String propName,String propValue){
		properties.put(propName, propValue);
	}
	
	public String getProperty(String propName){
		return properties.get(propName);
	}

	public Context getParent(){
		return _parent;
	}
}
