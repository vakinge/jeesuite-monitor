package com.jeesuite.monitor.core;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jeesuite.monitor.channel.KafkaUploadChannel;
import com.jeesuite.monitor.exception.ContextInitialException;
import com.jeesuite.monitor.model.config.ComponentDef;
import com.jeesuite.monitor.model.config.NodeDef;
import com.jeesuite.monitor.model.config.TaskDef;
import com.jeesuite.monitor.task.ServerStatusCollectorTask;
import com.jeesuite.monitor.task.ServiceConnectionCheckTask;

/**
 * 
 * 功能描述：监控上下文<br />
 *  
 * 创建日期：2013-6-25 上午11:29:51  <br />   
 * 
 * 版权信息：Copyright (c) 2013 Koala All Rights Reserved<br />
 * 
 * 作    者：<a href="mailto:vakinge@gmail.com">vakin jiang</a><br />
 * 
 * 修改记录： <br />
 * 修 改 者    修改日期     文件版本   修改说明
 */
public class MonitorContext extends AbstractContext {

	private static final Logger LOG = LoggerFactory.getLogger(MonitorContext.class);
	
	private static MonitorContext context;
	
	private TraceManager _container = null;

	private NodeDef _config=null;
	
	private Map<String,Component> _components = null;
	
	private Map<String,MonitorTask> _monitorTasks = null;
	

	private ConfigureManager _defManager=null;
	
	//监控数据缓存
	private UploadChannel channel;

	private MonitorContext(NodeDef config,TraceManager container,ConfigureManager defManager) {
		_components = new HashMap<String,Component>();
		_monitorTasks = new HashMap<String, MonitorTask>();
		_container = container;
		_config=config;
		_defManager=defManager;
		channel = new KafkaUploadChannel();
		LOG.info("当前监控配置为：\n"+config.toString());
	}
	
	public static MonitorContext registerContext(NodeDef config,ConfigureManager defManager){
		if(context == null){
			context = new MonitorContext(config,new TraceContainer(), defManager);
		}
		return context;
	}

	public static MonitorContext getContext() {
		if(context == null){
			throw new ContextInitialException("jwebap not startup successfully,please check the application log.");
		}
		return context;
	}

	public UploadChannel getChannel() {
		return channel;
	}

	/**
	 * 返回当前实例配置
	 * @return
	 */
	public NodeDef getNodeDef(){
		return _config;
	}
	
	/**
	 * 返回jwebap配置管理器
	 * @return
	 */
	public ConfigureManager getDefManager(){
		return _defManager;
	}
	
	public String getServerName() {
		return getProperty("serverName");
	}

	public String getDeployPath() {
		return getProperty("deployPath");
	}
	
	public void startup(){
		Collection<ComponentDef> components = _config.getComponents();
		Iterator<ComponentDef> componentIt=components.iterator();
		//启动数据通道
		channel.start();
		/**
		 * 注册Components
		 */
		while (componentIt.hasNext()) {			
			ComponentDef def = componentIt.next();
			//未激活则不启动
            if(!def.isActive())continue;
            registerComponent(def.getType(), def);

		}
		LOG.info("component startup finished. register Component Count:"+_components.size());
		
		//注册监控任务
		List<TaskDef> tasks = _config.getTasks();
		if(tasks != null && tasks.size()>0){
			for (TaskDef taskDef : tasks) {
				registerTask(taskDef);
			}
		}
		
		//注册数据同步服务
		registerDataProcessService();
	}

	/**
	 * 注册监控组件
	 * @param type
	 * @param def
	 * @throws RegisterException
	 */
	private void registerComponent(String type, ComponentDef def){
        try {
        	Component component = ComponentFactory.getInstance(type);
    		if(component == null){
    			LOG.warn("[{}][{}]无匹配插件，跳过",def.getName(),type);
    			return ;
    		}
    		
    		ComponentContext context = new StandardComponentContext(getContainer(), this, def);
    		component.startup(context);
    		_components.put(type, component);
    		context.setComponent(component);
    		context.initProperties(def.getProperties());
		} catch (Exception e) {
			e.printStackTrace();
			LOG.warn("[{}][{}]插件启动失败",def.getName(),type);
		}
	}
	
	/**
	 * 注册监控服务
	 * @param task
	 */
	private void registerTask(TaskDef taskdef){
		try {
			if(!taskdef.isActive())return;
			if(ServiceConnectionCheckTask.TASK_KEY.equals(taskdef.getType())){
				MonitorTask task = new ServiceConnectionCheckTask(taskdef);
				task.startup();
				_monitorTasks.put(taskdef.getType(), task);
				LOG.info("[{}][{}]启动 OK",taskdef.getName(),taskdef.getType());
			}else if(ServerStatusCollectorTask.TASK_KEY.equals(taskdef.getType())){
				MonitorTask task = new ServerStatusCollectorTask(taskdef);
				task.startup();
				_monitorTasks.put(taskdef.getType(), task);
				LOG.info("[{}][{}]启动 OK",taskdef.getName(),taskdef.getType());
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOG.warn("[{}][{}]启动 fail",taskdef.getName(),taskdef.getType());
		}
	}
	
	/**
	 * 注册数据处理服务
	 */
	private void registerDataProcessService(){
		if(_components.size() == 0)return;
		try {
			//TODO 
		} catch (Exception e) {
			LOG.error("监控数据同步服务启动失败",e);
		}
	}
	
	/**
	 * 注册服务器容器信息
	 */
	public void registerServerInfos(Map<String, String> serverInfos){
		initProperties(serverInfos);
	}

	public Component getComponent(String name) {
		return _components.get(name);
	}
	
	public ComponentContext getComponentContext(String name) {
		Component component = _components.get(name);
		return component == null ? null : component.getComponentContext();
	}
	
	public ComponentDef getComponentDef(String name){
		ComponentDef componentDef = getNodeDef().getComponentDef(name);
		return componentDef;
	}

	public void unregisterComponent(String name) {
		Component component =  _components.get(name);
		component.destory();
		_components.remove(name);
	}

	public TraceManager getContainer() {
		return _container;
	}

	public void setContainer(TraceManager container) {
		this._container = container;
	}

	/**
	 * RuntimeContext是组件的上下文的根
	 */
	public Context getParent() {
		return null;
	}
	
	/**
	 * 获取指定组件属性
	 * @param componentName
	 * @param propName
	 * @return
	 */
	public static String getComponentProps(String componentName,String propName){
		ComponentContext componentContext = getContext().getComponentContext(componentName);
		return componentContext == null ? null : componentContext.getProperty(propName);
	}
	
	
	/**
	 * 判断组件是否激活
	 * @param componentName
	 * @return
	 */
	public static boolean componentIsActive(String componentName){
		ComponentDef def = getContext().getComponentDef(componentName);
		if(def == null)return false;
		return def.isActive();
	}
	
	/**
	 * 获取指定监控任务
	 * @param taskKey
	 * @return
	 */
	public MonitorTask getMonitorTask(String taskKey) {
		return _monitorTasks.get(taskKey);
	}
}
