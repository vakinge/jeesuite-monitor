package com.jeesuite.monitor.core;


/**
 * Component Plug-in初始化上下文
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2016年6月15日
 */
public interface ComponentContext extends Context{
	
	/**
	 * 获取轨迹容器
	 * 
	 * @return
	 */
	public TraceManager getContainer();
	
	public Component getComponent();
	
	public void setComponent(Component component);
	
	
}
