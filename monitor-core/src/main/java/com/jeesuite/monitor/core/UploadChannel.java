package com.jeesuite.monitor.core;

import com.jeesuite.monitor.model.metaobject.Trace;

/**
 * 数据上传渠道接口
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2016年6月15日
 */
public interface UploadChannel {
	
	public void start();
	
	public void close();

	public void process(Trace trace);
}
