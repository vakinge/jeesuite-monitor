package com.jeesuite.monitor.constant;

public class MonitorContants {

	public static final String CONF_DRIVER_CLASS = "driver-clazzs";
	
	public static final String CONF_TRACE_TIMEOUT = "trace-timeout";
	
	public static final String CONF_TRACE_THRESHOLD = "trace-threshold";
	
	public static final String CONF_TRACE_STACK = "trace-stack";
	
	public static final String CONF_DETECT_PACKAGES = "detect-packages";
}
