package com.jeesuite.monitor.analyser;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jeesuite.monitor.constant.MonitorContants;
import com.jeesuite.monitor.core.Analyser;
import com.jeesuite.monitor.core.MonitorContext;
import com.jeesuite.monitor.core.TraceContainer;
import com.jeesuite.monitor.model.metaobject.MethodTrace;
import com.jeesuite.monitor.model.metaobject.Trace;
import com.jeesuite.monitor.model.metaobject.Trace.TraceType;
/**
 * 
 * 功能描述：通用分析器（耗时、超时等）<br />
 *  
 * 创建日期：2013-7-1 上午11:07:20  <br />   
 * 
 * 版权信息：Copyright (c) 2013 Koala All Rights Reserved<br />
 * 
 * 作    者：<a href="mailto:vakinge@gmail.com">vakin jiang</a><br />
 * 
 * 修改记录： <br />
 * 修 改 者    修改日期     文件版本   修改说明
 */
public class CommonAnalyser implements Analyser{
	
	private static final Logger LOG = LoggerFactory.getLogger(CommonAnalyser.class);

	
	protected String traceType;

	private List<Trace> traces;
	
	private Timer checkTimer;
	
	//每个主线程调用的第一个方法集合
	private static Map<String, String> methodEnpoints = new ConcurrentHashMap<String, String>();

	public CommonAnalyser(String traceType) {
		this.traceType = traceType;
		this.traces = new Vector<Trace>();
		
		checkTimer = new Timer();
		
		checkTimer.schedule(new TimerTask() {
			
			@Override
			public void run() {
				checkTimeoutTrace();
			}
		}, getTraceTimeout());
	}
	
	
	
	public void activeProcess(Trace trace) {
		traces.add(trace);
		//记录入口方法
		if(TraceType.METHOD.name().equals(trace.getTraceType())){
			if(!methodEnpoints.containsKey(trace.getThreadId())){
				methodEnpoints.put(trace.getThreadId(), ((MethodTrace)trace).getMethod());
			}
		}
	
	}

	public void inactiveProcess(Trace trace) {
		long activeTime = System.currentTimeMillis() - trace.getBeginTime();
		long threshold = getTraceThreshold();
		//如果存活时间小于监控的最小阀值
		if (threshold >= activeTime){
			//发生异常的方法也需要继续处理
			if(TraceType.METHOD.name().equals(trace.getTraceType()) && ((MethodTrace)trace).isSuccessed()==false){
				trace.inActive();
			}else{
				trace.destroy();
			}
		}else{
			trace.inActive();
		}
		traces.remove(trace);
		
		//http请求结束 释放
		if(TraceType.HTTP.name().equals(trace.getTraceType())){
			methodEnpoints.remove(trace.getThreadId());
			TraceContainer.clearThreadKey();
		}
	}
	
	public void destoryProcess(Trace trace) {
				
	}
	
	/**
	 * 清空统计数据
	 *
	 */
	public void clear() {
		traces.clear();	
	}

	
	private long getTraceThreshold() {
		String value = MonitorContext.getComponentProps(traceType, MonitorContants.CONF_TRACE_THRESHOLD);
		try {
			return Long.parseLong(value);
		} catch (Exception e) {
			return -1;
		}
	}
	
	private long getTraceTimeout() {
		String value = MonitorContext.getComponentProps(traceType, MonitorContants.CONF_TRACE_TIMEOUT);
		try {
			return Long.parseLong(value);
		} catch (Exception e) {
			return 60*1000;
		}
	}
	
	public static String getTraceEndpointMethod(String traceKey){
		return methodEnpoints.get(traceKey);
	}

	public static void removeTraceEndpointMethod(String traceKey){
		methodEnpoints.remove(traceKey);
	}
	
	/**
	 * 检查超时trace
	 */
	private void checkTimeoutTrace() {
		try {
			Iterator<Trace> iterator = traces.iterator();
			while (iterator.hasNext()) {
				Trace trace = iterator.next();
				if (System.currentTimeMillis() - trace.getBeginTime() > getTraceTimeout()) {
					trace.setTimeout(true);
					trace.inActive();
					methodEnpoints.remove(trace.getThreadId());
					MonitorContext.getContext().getContainer().inactivateTrace(traceType, trace);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOG.warn("处理超时trace发生错误:{}", e.getMessage());
		}
	}
}