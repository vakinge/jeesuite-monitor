package com.jeesuite.monitor.exception;


public class ContextInitialException  extends RuntimeException {

	static final long serialVersionUID = -19811222L;
	
	public ContextInitialException(String message) {
		super(message);
	}

	public ContextInitialException(String message, Throwable cause) {
		super(message, cause);
	}

}
