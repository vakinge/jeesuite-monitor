package com.jeesuite.monitor.exception;


public class PluginDefParseException extends Exception {
	
	static final long serialVersionUID = -1L;
	
	public PluginDefParseException(String message) {
		super(message);
	}

	public PluginDefParseException(String message, Throwable cause) {
		super(message, cause);
	}
}
