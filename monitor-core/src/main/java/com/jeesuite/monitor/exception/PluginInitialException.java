package com.jeesuite.monitor.exception;


public class PluginInitialException extends RuntimeException {
	
	static final long serialVersionUID = -1L;
	
	public PluginInitialException(String message) {
		super(message);
	}

	public PluginInitialException(String message, Throwable cause) {
		super(message, cause);
	}
}
