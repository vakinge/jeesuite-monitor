package com.jeesuite.monitor.exception;



public class PluginDefNotFoundException extends Exception {
	
	static final long serialVersionUID = -1L;
	
	public PluginDefNotFoundException(String message) {
		super(message);
	}

	public PluginDefNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
}
