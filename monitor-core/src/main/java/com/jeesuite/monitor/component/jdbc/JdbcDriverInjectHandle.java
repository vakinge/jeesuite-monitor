package com.jeesuite.monitor.component.jdbc;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.sql.Connection;

import javax.sql.DataSource;

import com.jeesuite.monitor.common.asm.MethodInjectHandler;
import com.jeesuite.monitor.core.TraceManager;

/**
 * 数据库监控注入handle
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2016年6月15日
 */
public class JdbcDriverInjectHandle implements MethodInjectHandler {

	/**
	 * 运行轨迹容器
	 */
	private transient TraceManager _container = null;

	public JdbcDriverInjectHandle(TraceManager container) {
		_container = container;
	}

	public Object invoke(Object target, Method method, Method methodProxy,
			Object[] args) throws Throwable {
		Object o;
		try {
			o = methodProxy.invoke(target, args);
		} catch (InvocationTargetException e) {
			// 抛出原有异常
			throw e.getCause();
		}
		if (!Modifier.isPrivate(method.getModifiers())
				&& o instanceof Connection
				&& !(o instanceof ProxyConnection)) {
			return new ProxyConnection(_container, (Connection) o,(DataSource)target);
		} else if (!Modifier.isPrivate(method.getModifiers())
				&& o instanceof DataSource
				&& !(o instanceof ProxyDataSource)) {
			return new ProxyDataSource(_container, (DataSource) o,(DataSource)target);
		} else {
			return o;
		}

	}

}
