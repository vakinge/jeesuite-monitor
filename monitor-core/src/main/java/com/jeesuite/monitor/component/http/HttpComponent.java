package com.jeesuite.monitor.component.http;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jeesuite.monitor.analyser.CommonAnalyser;
import com.jeesuite.monitor.analyser.SessionFilterAnalyser;
import com.jeesuite.monitor.component.AbstractComponent;
import com.jeesuite.monitor.core.ComponentContext;
import com.jeesuite.monitor.core.TraceManager;
import com.jeesuite.monitor.model.metaobject.Trace.TraceType;

/**
 *  http请求监控组件
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2016年6月15日
 */
public class HttpComponent extends AbstractComponent {
	private static final Logger log = LoggerFactory.getLogger(HttpComponent.class);

	private ComponentContext componentContext = null;

	public void startup(ComponentContext context) {
		componentContext = context;
		TraceManager container = componentContext.getContainer();
		container.registerAnalyser(TraceType.HTTP.name(), new CommonAnalyser(TraceType.HTTP.name()));
		container.registerAnalyser(TraceType.HTTP.name(), new SessionFilterAnalyser(TraceType.HTTP.name()));
		log.info("httpcomponent startup.");
	}

	public void destory() {
	}

	public void clear() {}

	public ComponentContext getComponentContext() {
		return componentContext;
	}
}
