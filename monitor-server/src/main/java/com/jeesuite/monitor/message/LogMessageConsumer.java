/**
 * 
 */
package com.jeesuite.monitor.message;

import java.io.Closeable;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jeesuite.monitor.model.message.KafkaMessage;

/**
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2016年11月8日
 */
public class LogMessageConsumer implements Closeable{

	private static final Logger logger = LoggerFactory.getLogger(LogMessageConsumer.class);

	private Properties configs;
	
	private KafkaConsumer<String, KafkaMessage> consumer;
	private ExecutorService fetcheExecutor;
	private ThreadPoolExecutor processExecutor;

	private String topic = "_monitor_log_topic";
	
	private AtomicBoolean closed = new AtomicBoolean();

	public void start() {
		fetcheExecutor = Executors.newFixedThreadPool(1);
		processExecutor = new ThreadPoolExecutor(10, 10,
                0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>());
		createKafkaConsumer();
		ConsumerWorker consumer = new ConsumerWorker();
		fetcheExecutor.submit(consumer);
	}

	@Override
	public void close() {
		closed.set(true);
		fetcheExecutor.shutdown();
		processExecutor.shutdown();
		consumer.close();
	}
	
	final Map<TopicPartition, Long> partitionToUncommittedOffsetMap = new ConcurrentHashMap<>();
	final List<Future<Boolean>> futures = new ArrayList<>();
	
	private <K extends Serializable, V extends KafkaMessage> void createKafkaConsumer(){
		consumer = new KafkaConsumer<>(configs);
		ConsumerRebalanceListener listener = new ConsumerRebalanceListener() {

			@Override
			public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
				if (!futures.isEmpty())
					futures.get(0).cancel(true);

				commitOffsets(partitionToUncommittedOffsetMap);
			}

			@Override
			public void onPartitionsAssigned(Collection<TopicPartition> partitions) {
				for (TopicPartition tp : partitions) {
					OffsetAndMetadata offsetAndMetaData = consumer.committed(tp);
					long startOffset = offsetAndMetaData != null ? offsetAndMetaData.offset() : -1L;
					logger.info("Assigned topicPartion : {} offset : {}", tp, startOffset);

					if (startOffset >= 0)
						consumer.seek(tp, startOffset);
				}
			}
		};

		List<String> topics = new ArrayList<>(Arrays.asList(topic));
		consumer.subscribe(topics, listener);
	}
	
	
	private void commitOffsets(Map<TopicPartition, Long> partitionToOffsetMap) {

		if (!partitionToOffsetMap.isEmpty()) {
			Map<TopicPartition, OffsetAndMetadata> partitionToMetadataMap = new HashMap<>();
			for (Entry<TopicPartition, Long> e : partitionToOffsetMap.entrySet()) {
				partitionToMetadataMap.put(e.getKey(), new OffsetAndMetadata(e.getValue() + 1));
			}

			logger.info("committing the offsets : {}", partitionToMetadataMap);
			consumer.commitSync(partitionToMetadataMap);
			partitionToOffsetMap.clear();
		}
	}
	
	private class ConsumerWorker implements Runnable {

		@Override
		public void run() {

			ExecutorService executor = Executors.newFixedThreadPool(1);

			while (!closed.get()) {

				ConsumerRecords<String,KafkaMessage> records = consumer.poll(1500);

				// no record found
				if (records.isEmpty()) {
					continue;
				}

				//由于处理消息可能产生延时，收到消息后，暂停所有分区并手动发送心跳，以避免consumer group被踢掉
				consumer.pause(consumer.assignment());
				Future<Boolean> future = executor.submit(new ConsumeRecords(records, partitionToUncommittedOffsetMap));
				futures.add(future);

				Boolean isCompleted = false;
				while (!isCompleted && !closed.get()) {
					try {
						//等待 heart-beat 间隔时间
						isCompleted = future.get(3, TimeUnit.SECONDS); 
					} catch (TimeoutException e) {
						logger.debug("heartbeats the coordinator");
						consumer.poll(0); // does heart-beat
						commitOffsets(partitionToUncommittedOffsetMap);
					} catch (CancellationException e) {
						logger.debug("ConsumeRecords Job got cancelled");
						break;
					} catch (ExecutionException | InterruptedException e) {
						logger.error("Error while consuming records", e);
						break;
					}
				}
				futures.remove(future);
				consumer.resume(consumer.assignment());
				commitOffsets(partitionToUncommittedOffsetMap);
			}

			try {
				executor.shutdownNow();
				while (!executor.awaitTermination(5, TimeUnit.SECONDS))
					;
			} catch (InterruptedException e) {
				logger.error("Error while exiting the consumer");
			}
			consumer.close();
			logger.info("C : {}, consumer exited");
		}

		private class ConsumeRecords implements Callable<Boolean> {

			ConsumerRecords<String,KafkaMessage> records;
			Map<TopicPartition, Long> partitionToUncommittedOffsetMap;

			public ConsumeRecords(ConsumerRecords<String,KafkaMessage> records,
					Map<TopicPartition, Long> partitionToUncommittedOffsetMap) {
				this.records = records;
				this.partitionToUncommittedOffsetMap = partitionToUncommittedOffsetMap;
			}

			@Override
			public Boolean call() {

				logger.info("Number of records received : {}", records.count());
				try {
					for (final ConsumerRecord<String,KafkaMessage> record : records) {
						TopicPartition tp = new TopicPartition(record.topic(), record.partition());
						logger.info("Record received topicPartition : {}, offset : {}", tp,record.offset());
						partitionToUncommittedOffsetMap.put(tp, record.offset());
						//第二阶段处理
						processExecutor.submit(new Runnable() {
							@Override
							public void run() {
								processMessage(record.value());
							}
						});
					}
				} catch (Exception e) {
					logger.error("Error while consuming", e);
				}
				return true;
			}
			
			private void processMessage(KafkaMessage message){
				
			}
		}

	}

}
